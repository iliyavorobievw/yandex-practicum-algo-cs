using System;
using System.Linq;

class Program
{
    static void Main()
    {
        string[] input = Console.ReadLine().Split();
        int n = int.Parse(input[0]);
        int m = int.Parse(input[1]);

        int[][] matrix = new int[n + 1][];
        int[][] dp = new int[n + 2][];

        for (int i = 0; i < n + 1; i++)
        {
            matrix[i] = new int[m + 1];
            dp[i] = new int[m + 1];
        }
        dp[n + 1] = new int[m + 1];

        for (int i = 1; i < n + 1; i++)
        {
            string row = Console.ReadLine();
            for (int g = 0; g < m; g++)
            {
                matrix[i][g + 1] = row[g] - '0';
            }
        }

        for (int i = n; i > 0; i--)
        {
            for (int g = 1; g < m + 1; g++)
            {
                dp[i][g] = Math.Max(dp[i + 1][g], dp[i][g - 1]) + matrix[i][g];
            }
        }

        Console.WriteLine(dp[1][m]);
    }
}
