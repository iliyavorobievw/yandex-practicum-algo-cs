using System;
using System.Linq;

class Program
{
// и в пролет не брошусь и не выпью яда 
    static void Main()
    {
        int x = Convert.ToInt32(Console.ReadLine());
        int k = Convert.ToInt32(Console.ReadLine());
        int[] denominations = Console.ReadLine().Split().Select(int.Parse).ToArray();

        int[] dp = new int[x + 1];
        Array.Fill(dp, int.MaxValue);
        dp[0] = 0;

        for (int i = 0; i < k; i++)
        {
            for (int j = denominations[i]; j <= x; j++)
            {
                if (dp[j - denominations[i]] != int.MaxValue)
                {
                    dp[j] = Math.Min(dp[j], dp[j - denominations[i]] + 1);
                }
            }
        }

        Console.WriteLine(dp[x] == int.MaxValue ? -1 : dp[x]);
    }
}
