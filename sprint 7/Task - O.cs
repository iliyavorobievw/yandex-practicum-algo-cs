using System;
using System.Collections.Generic;
using System.Linq;

public class Vertex
{
    public int Value { get; set; }
    public List<Vertex> PointsTo { get; set; } = new List<Vertex>();
}

public class Program
{
    private const int MOD = 1000000007;

    public static void Main()
    {
        var input = Console.ReadLine().Split(' ').Select(int.Parse).ToArray();
        var n = input[0];
        var m = input[1];

        var list = Enumerable.Range(0, n + 1).Select(i => new Vertex { Value = i }).ToArray();

        for (var i = 0; i < m; i++)
        {
            var edge = Console.ReadLine().Split(' ').Select(int.Parse).ToArray();
            list[edge[0]].PointsTo.Add(list[edge[1]]);
        }

        var path = Console.ReadLine().Split(' ').Select(int.Parse).ToArray();
        var from = path[0];
        var to = path[1];

        var paths = new int[n + 1];
        var visited = new bool[n + 1];
        paths[to] = 1;

        Dfs(list[from], visited, paths);
        Console.WriteLine(paths[from]);
    }

    private static void Dfs(Vertex vertex, bool[] visited, int[] paths)
    {
        visited[vertex.Value] = true;
        foreach (var v in vertex.PointsTo)
        {
            if (!visited[v.Value])
            {
                Dfs(v, visited, paths);
            }
            paths[vertex.Value] = (paths[vertex.Value] + paths[v.Value]) % MOD;
        }
    }
}