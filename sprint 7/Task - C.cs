using System;
using System.Linq;
/// comment ti 
public class Program
{
    public static void Main()
    {
        int M = int.Parse(Console.ReadLine());
        int n = int.Parse(Console.ReadLine());

        var piles = new (int c, int m)[n];
        for (int i = 0; i < n; i++)
        {
            var input = Console.ReadLine().Split(' ').Select(int.Parse).ToArray();
            piles[i] = (input[0], input[1]);
        }

        piles = piles.OrderByDescending(p => p.c).ToArray();

        long totalValue = 0;
        foreach (var pile in piles)
        {
            if (M >= pile.m)
            {
                totalValue += (long)pile.c * pile.m;
                M -= pile.m;
            }
            else
            {
                totalValue += (long)pile.c * M;
                break;
            }
        }
        /// kill me

        Console.WriteLine(totalValue);
    }
}
