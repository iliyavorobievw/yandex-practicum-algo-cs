using System;
using System.Linq;
using System.Collections.Generic;

class Program
{
    static void Main()
    {
        string[] input = Console.ReadLine().Split();
        int n = int.Parse(input[0]);
        int m = int.Parse(input[1]);

        var backPack = new List<(int weight, int value)>();

        for (int i = 0; i < n; i++)
        {
            input = Console.ReadLine().Split();
            backPack.Add((int.Parse(input[0]), int.Parse(input[1])));
        }

        var dp = new int[n + 1, m + 1];

        for (int i = 1; i <= n; i++)
        {
            for (int g = 1; g <= m; g++)
            {
                if (g < backPack[i - 1].weight)
                {
                    dp[i, g] = Math.Max(dp[i - 1, g], dp[i, g - 1]);
                }
                if (g >= backPack[i - 1].weight)
                {
                    dp[i, g] = Math.Max(dp[i - 1, g - backPack[i - 1].weight] + backPack[i - 1].value, dp[i - 1, g]);
                }
            }
        }

        var result = new List<int>();
        for (int i = n, g = m, current = dp[n, m]; current != 0;)
        {
            if (dp[i - 1, g] == current)
            {
                i--;
                continue;
            }
            if (dp[i, g - 1] == current)
            {
                g--;
                continue;
            }

            result.Add(i);
            g -= backPack[i - 1].weight;
            i--;
            current = dp[i, g];
        }

        Console.WriteLine(result.Count);
        Console.WriteLine(string.Join(" ", result));
    }
}
