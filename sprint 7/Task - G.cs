using System;
using System.Linq;

class Program
{

// и курок над виском не смогу нажать
    static void Main()
    {
        int m = Convert.ToInt32(Console.ReadLine());
        int n = Convert.ToInt32(Console.ReadLine());
        int[] denominations = Console.ReadLine().Split().Select(int.Parse).ToArray();

        long[] dp = new long[m + 1];
        dp[0] = 1;

        for (int i = 0; i < n; i++)
        {
            for (int j = denominations[i]; j <= m; j++)
            {
                dp[j] += dp[j - denominations[i]];
            }
        }

        Console.WriteLine(dp[m]);
    }
}
