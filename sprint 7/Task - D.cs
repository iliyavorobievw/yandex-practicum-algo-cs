using System;

public class Program
{
    public static void Main()
    {
        int n = int.Parse(Console.ReadLine());
        Console.WriteLine(Fibonacci(n));
    }

    public static int Fibonacci(int n)
    {
        const int MOD = 1000000007;
        if (n <= 1)
        {
            return 1;
        }

        int[] fib = new int[n + 1];
        fib[0] = fib[1] = 1;

        for (int i = 2; i <= n; i++)
        {
            fib[i] = (fib[i - 1] + fib[i - 2]) % MOD; 
        }

        return fib[n];
    }
}
