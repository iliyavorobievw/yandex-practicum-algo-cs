using System;
using System.Linq;

class Program
{
	// report-link - https://contest.yandex.ru/contest/25597/run-report/92549024/
	
	/*
	
	алгоритм считывает количество выигранных партий и заработанные в партиях очки.
	далее вычисляет общую сумму очков.
	Если общая сумма нечетная, то разделение на два равных подмножества невозможно, поэтому  выводит “False” и завершается.
	Если общая сумма четная,  проверяет, можно ли разделить очки на два подмножества с одинаковой суммой, используя алгоритм динамического программирования.
	В конце вывод “True”, если разделение возможно, и “False” в противном случае.
	
	Временная сложность - O(n^2),
	где n - общее количество очков.
	алгоритм проходит по каждому элементу массива для каждого возможного значения суммы.
	
	Пространственная сложность - O(n),
	где n - общее количество очков.
	алгоритм использует одномерный массив для хранения промежуточных результатов.
	*/
	
    static void Main()
    {
        // Считываем количество выигранных партий
        var gamesWon = int.Parse(Console.ReadLine());

        // Считываем заработанные в партиях очки
        var gamePoints = Array.ConvertAll(Console.ReadLine().Split(), int.Parse);

        // Вычисляем сумму очков
        var totalPoints = gamePoints.Sum();

        // Если сумма нечетная или не проходит проверку, выводим "False" и завершаем программу
        if (totalPoints % 2 != 0 || !IsSumPossible(gamesWon, totalPoints, gamePoints))
        {
            Console.WriteLine("False");
            return;
        }

        // В противном случае выводим "True"
        Console.WriteLine("True");
    }

    static bool IsSumPossible(int gamesWon, int totalPoints, int[] gamePoints)
    {
        var previousDp = new int[totalPoints / 2 + 1];
        var currentDp = new int[totalPoints / 2 + 1];

        for (var i = 1; i < gamesWon + 1; i++)
        {
            for (var j = 1; j < totalPoints / 2 + 1; j++)
            {
                currentDp[j] = previousDp[j];
                if (j == gamePoints[i - 1])
                {
                    currentDp[j]++;
                }
                if (j > gamePoints[i - 1] && previousDp[j - gamePoints[i - 1]] > 0)
                {
                    currentDp[j]++;
                }
            }
            previousDp = (int[])currentDp.Clone();
            currentDp = new int[totalPoints / 2 + 1];
        }

        return previousDp[totalPoints / 2] > 1;
    }
}
