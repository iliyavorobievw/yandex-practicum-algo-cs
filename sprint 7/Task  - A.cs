using System;
using System.Collections.Generic;
using System.Linq;

public class Program
{
    public static void Main()
    {
        Console.ReadLine();

        var prices = Console.ReadLine().Split(' ').Select(x => Convert.ToInt32(x)).ToList(); // Замените этот массив на свои данные
        Console.WriteLine(MaxProfit(prices));
    }

    public static int MaxProfit(List<int> prices)
    {
        int maxProfit = 0;
        for (int i = 1; i < prices.Count; i++)
        {
            if (prices[i] > prices[i - 1])
            {
                maxProfit += prices[i] - prices[i - 1];
            }
        }
        return maxProfit;
    }
}
