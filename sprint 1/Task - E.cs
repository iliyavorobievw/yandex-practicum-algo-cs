using System;
using System.Linq;
using System.Collections;
using System.Collections.Generic;
using System.Security.Cryptography.X509Certificates;

public class Program
{
    static void Main(string[] args)
    {
        Console.ReadLine();
        var someStr = Console.ReadLine();

        var list = someStr.Trim()
                          .Split(' ')
                          .ToList();

        Console.WriteLine($"{list.MaxBy(x => x.Length)} \n{list.MaxBy(x => x.Length).Length}"); 

    }
}
