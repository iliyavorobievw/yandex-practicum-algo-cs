using System;
using System.Linq;
public class Program
{
    static void Main(string[] args)
    {
        var listNumber = Console.ReadLine().Split(' ')
                                     .Select(e => Convert.ToInt32(e))
                                     .ToList();
        var listBool = listNumber.Select(x => x % 2 == 0).ToList<bool>();
        if (listBool.Contains(true) && listBool.Contains(false))
        {
            Console.WriteLine("FAIL");
        }
        else
        {
            Console.WriteLine("WIN");
        }

    }
}
