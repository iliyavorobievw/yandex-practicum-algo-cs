using System;
using System.Linq;
using System.Collections;
using System.Collections.Generic;
public class Program
{
    static void Main(string[] args)
    {
        var number = Convert.ToInt32(Console.ReadLine());

        var stackInt = new Stack<int>();

        if (number == 0)
        {
            Console.WriteLine("0");
            return;
        }
        while (number >= 1)
        {
            stackInt.Push(number%2);
            number = number / 2;
        }
        foreach (var item in stackInt)
        {
            Console.Write(item);
        }

    }
}
