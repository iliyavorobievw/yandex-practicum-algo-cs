using System;
using System.Linq;


    public class Program
    {
        static void Main(string[] args)
        {
            int[] arrayNumber = Console.ReadLine().Split(' ')
                                         .Select(e => Convert.ToInt32(e))
                                         .ToArray();


            Console.WriteLine(arrayNumber[0] * Math.Pow(arrayNumber[1], 2) + arrayNumber[2] * arrayNumber[1] + arrayNumber[3]);
        }
    }
