using System;
using System.Linq;
using System.Collections;
using System.Collections.Generic;
using System.Security.Cryptography.X509Certificates;
using System.Globalization;

public class Program
{
    static void Main(string[] args)
    {
        Console.ReadLine();
        var array = Console.ReadLine().Split(" ")
                                      .Select(x => Convert.ToInt32(x))
                                      .ToList();
        SearchHomePlaceForTimofey(array);
        Console.WriteLine(String.Join(" ", array));
    }

    static void SearchHomePlaceForTimofey(List<int> array)
    {
       // Console.WriteLine(String.Join(" ", array));

        var indexLastZero = -1;
        var iterator = 0;
        for (int i = 0; i < array.Count; i++)
        {
            if (array[i] == 0)
            {
                indexLastZero = i;
                array[i] = 0;
                iterator = 0;
                continue;
            }
            if (indexLastZero < 0)
            {
                array[i] = int.MaxValue;
                continue;
            }
            array[i] = ++iterator;
        }
        iterator = 0;
        for (int i = array.Count - 1; i >= 0; i--)
        {
            if (array[i] == 0)
            {
                indexLastZero = i;
                array[i] = 0;
                iterator = 0;
                continue;
            }
            iterator++;
            if (array[i] > iterator)
            {
                array[i] = Math.Abs(i - indexLastZero);
            }

        }
    }
}