using System;
using System.Linq;
using System.Collections;
using System.Collections.Generic;
using System.Security.Cryptography.X509Certificates;
using System.Globalization;

public class Program
{
    static void Main(string[] args)
    {
        var maxFingerOnePerson = int.Parse(Console.ReadLine());
        var str = "";
        var score = 0;
        for (int i = 0; i < 4; i++)
        {
            str += Console.ReadLine();
        }

        var listDigit = new List<int>();
        listDigit = str.Where(x => char.IsDigit(x))
                       .Select(x => int.Parse(x.ToString()))
                       .ToList();

        for (int i = 0; i <= 9; i++)
        {
            if (listDigit.Count(x => x == i) <= maxFingerOnePerson * 2)
            {
                if (listDigit.Contains(i))
                {
                    score++;
                }
                
            }

        }

        Console.WriteLine(score);
    }
}