using System;
using System.Linq;
using System.Collections;
using System.Collections.Generic;
using System.Security.Cryptography.X509Certificates;

public class Program
{
    static void Main(string[] args)
    {
        var firstStr = Console.ReadLine().OrderBy(x => x).ToList();
        var secondstr = Console.ReadLine().OrderBy(x => x).ToList();


        for (var i = 0; i < firstStr.Count(); i++)
        {
            if (firstStr[i] != secondstr[i])
            {
                Console.WriteLine(secondstr[i]);
                return;
            }
            
        }
        Console.WriteLine(secondstr.Last());

    }
}
