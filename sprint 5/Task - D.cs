﻿using System;

public class Solution
{
    public static bool Solve(Node fNode, Node sNode)
    {
        if (fNode == null && sNode == null)
        {
            return true;
        }
        if (fNode == null || sNode == null)
        {
            return false;
        }
        if (fNode.Value != sNode.Value)
        {
            return false;
        }

        return Solve(fNode.Left, sNode.Left) 
            && Solve(fNode.Right, sNode.Right);
    }
}
/*
public class Node
{
    public int Value { get; set; }
    public Node Left { get; set; }
    public Node Right { get; set; }

    public Node(int value)
    {
        Value = value;
        Left = null;
        Right = null;
    }
}
*/