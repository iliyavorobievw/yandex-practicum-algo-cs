﻿using System;
public class Solution
{
    public static int Solve(Node root)
    {
        return SumNode(root, 0);
    }

    private static int SumNode(Node node, int sum)
    {
        if (node == null)
        {
            return 0;
        }

        sum = sum * 10 + node.Value;

        if (node.Left == null && node.Right == null)
        {
            return sum;
        }

        return SumNode(node.Left, sum) + SumNode(node.Right, sum);
    }
}

/*
// закомментируйте перед отправкой 
public class Node
{
    public int Value { get; set; }
    public Node Left { get; set; }
    public Node Right { get; set; }

    public Node(int value)
    {
        Value = value;
        Left = null;
        Right = null;
    }
}
*/