﻿using System;
public class Solution
{
    public static bool Solve(Node root)
    {
        return CheckAnagram(root, root);
    }

    private static bool CheckAnagram(Node leftNode, Node rightNode)
    {
        if (leftNode == null && rightNode == null)
        {
            return true;
        }
        if (leftNode == null || rightNode == null)
        {
            return false;
        }

        return (leftNode.Value == rightNode.Value)
                && CheckAnagram(leftNode.Left, rightNode.Right)
                && CheckAnagram(leftNode.Right, rightNode.Left);
    }
}
/*
public class Node
{
    public int Value { get; set; }
    public Node Left { get; set; }
    public Node Right { get; set; }

    public Node(int value)
    {
        Value = value;
        Left = null;
        Right = null;
    }
}
*/
