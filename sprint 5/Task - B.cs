﻿using System;
public class Solution
{
    public static bool Solve(Node root)
    {
        return ViewNode(root) != int.MinValue;
    }

    private static int ViewNode(Node node)
    {
        if (node == null) return -1;

        int leftHeight = ViewNode(node.Left);

        if (leftHeight == int.MinValue)
        {
            return int.MinValue;
        }

        int rightHeight = ViewNode(node.Right);

        if (rightHeight == int.MinValue)
        {
            return int.MinValue;
        }

        if (Math.Abs(leftHeight - rightHeight) > 1)
        {
            return int.MinValue;
        }
        else
        {
            return Math.Max(leftHeight, rightHeight) + 1;
        }
    }
}