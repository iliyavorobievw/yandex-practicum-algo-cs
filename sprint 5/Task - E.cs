﻿using System;

public class Solution
{
    public static bool Solve(Node root)
    {
        return IsBinarySearchTree(root, int.MinValue, int.MaxValue);
    }

    private static bool IsBinarySearchTree(Node node, int min, int max)
    {
        if (node == null)
            return true;
        if (node.Value < min || node.Value > max)
            return false;
        return IsBinarySearchTree(node.Left, min, node.Value - 1) && IsBinarySearchTree(node.Right, node.Value + 1, max);
    }
}
