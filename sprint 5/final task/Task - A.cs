using System;
using System.Collections.Generic;

namespace HeapSort
{
    /// <summary>
    /// 
    /// report_link - https://contest.yandex.ru/contest/24810/run-report/88953276/
    /// 
    /// Этот код реализует сортировку кучей для списка участников соревнования. 
    /// Алгоритм сортировки кучей состоит из двух основных шагов:
    /// 
    /// 1) Построение кучи из исходного массива. Этот шаг выполняется с помощью алгоритма (sift-down),
    /// который перемещает элементы в массиве таким образом, чтобы они удовлетворяли свойству кучи. 
    /// 
    /// 2) Извлечение максимального(или минимального) элемента из кучи и его перемещение в конец массива.
    /// Затем размер кучи уменьшается на 1, и процесс повторяется до тех пор, пока в куче не останется элементов.
    ///
    /// Сначала участники сортируются по количеству решенных задач в порядке убывания.
    /// При равенстве числа решенных задач участники сортируются по штрафу в порядке возрастания.
    /// Если же и штрафы совпадают, то участники сортируются по логину
    /// 
    /// Temporary stability (Временная сложность) алгоритма сортировки кучей -  O(n log n), где n - количество элементов в списке.
    /// 
    /// Пространственная сложность алгоритма - O(1),
    /// так как алгоритм выполняет сортировку “на месте” без использования дополнительной памяти.
    /// 
    /// 
    /// </summary>
    class Program
    {
        static void Main(string[] args)
        {
            int n = int.Parse(Console.ReadLine());
            List<Participant> participants = new List<Participant>();
            for (int i = 0; i < n; i++)
            {
                string[] input = Console.ReadLine().Split();
                string login = input[0];
                int solvedTasks = int.Parse(input[1]);
                int penalty = int.Parse(input[2]);
                participants.Add(new Participant(login, solvedTasks, penalty));
            }

            SortHeap(participants, new ParticipantComparer());

            foreach (var participant in participants)
            {
                Console.WriteLine(participant.Login);
            }
        }

        /// <summary>
        /// Сортирует список участников соревнования с помощью алгоритма сортировки кучей.
        /// </summary>
        /// <param name="participants">Список участников соревнования.</param>
        /// <param name="comparer">Компаратор для сравнения участников.</param>
        public static void SortHeap(List<Participant> participants, IComparer<Participant> comparer)
        {
            // Построение кучи
            for (int i = participants.Count / 2 - 1; i >= 0; i--)
            {
                RestoreHeap(participants, participants.Count, i, comparer);
            }

            // Извлечение элементов из кучи
            for (int i = participants.Count - 1; i >= 0; i--)
            {
                Swap(participants, 0, i);
                RestoreHeap(participants, i, 0, comparer);
            }
        }

        /// <summary>
        /// Восстанавливает свойство кучи для поддерева с root в node i.
        /// </summary>
        /// <param name="participants">Список участников соревнования.</param>
        /// <param name="n">Размер кучи.</param>
        /// <param name="i">Индекс корня поддерева.</param>
        /// <param name="comparer">Компаратор для сравнения участников.</param>
        public static void RestoreHeap(List<Participant> participants, int n, int i, IComparer<Participant> comparer)
        {
            int iNode = i;
            int left = 2 * i + 1;
            int right = 2 * i + 2;

            if (left < n && comparer.Compare(participants[left], participants[iNode]) > 0)
            {
                iNode = left;
            }

            if (right < n && comparer.Compare(participants[right], participants[iNode]) > 0)
            {
                iNode = right;
            }

            if (iNode != i)
            {
                Swap(participants, i, iNode);
                RestoreHeap(participants, n, iNode, comparer);
            }
        }

        /// <summary>
        /// Обменивает местами два элемента списка участников.
        /// </summary>
        /// <param name="participants">Список участников соревнования.</param>
        /// <param name="a">Индекс первого элемента.</param>
        /// <param name="b">Индекс второго элемента.</param>
        public static void Swap(List<Participant> participants, int a, int b)
        {
            Participant temp = participants[a];
            participants[a] = participants[b];
            participants[b] = temp;
        }
    }
    /// <summary>
    /// Класс участника
    /// </summary>
    class Participant
    {
        /// <summary>
        /// Логин
        /// </summary>
        public string Login { get; set; }
        /// <summary>
        /// Решеные задачи
        /// </summary>
        public int SolvedTasks { get; set; }
        /// <summary>
        /// Штраф
        /// </summary>
        public int Penalty { get; set; }

        /// <summary>
        /// ctor
        /// </summary>
        /// <param name="login">логин</param>
        /// <param name="solvedTasks">решенные задачи</param>
        /// <param name="penalty">штраф</param>
        public Participant(string login, int solvedTasks, int penalty)
        {
            Login = login;
            SolvedTasks = solvedTasks;
            Penalty = penalty;
        }
    }

    class ParticipantComparer : IComparer<Participant>
    {
        public int Compare(Participant x, Participant y)
        {
            if (x.SolvedTasks != y.SolvedTasks)
            {
                return y.SolvedTasks.CompareTo(x.SolvedTasks);
            }
            else if (x.Penalty != y.Penalty)
            {
                return x.Penalty.CompareTo(y.Penalty);
            }
            else
            {
                return x.Login.CompareTo(y.Login);
            }
        }
    }
}
