﻿using System;

/// <summary>
/// 
///  report_link https://contest.yandex.ru/contest/24810/run-report/88963462/
/// time сложность удаления узла из bTree поиска - O(h), где h - высота tree.
/// 
/// memory сложность - O(h), тк алгоритм использует стек вызовов рекурсии.
/// 
/// Функция использует рекурсивный подход для поиска узла с заданным ключом и его удаления.
/// Если удаляемый узел имеет только одного потомка,
/// то этот потомок заменяет удаляемый узел.
/// Если удаляемый узел имеет двух потомков,
/// то его значение заменяется наименьшим значением в правом поддереве,
/// а затем этот наименьший элемент удаляется из правого поддерева.
/// </summary>
public class Solution
{
    /// <summary>
    /// Удаляет узел с заданным ключом из бинарного дерева поиска.
    /// </summary>
    /// <param name="root">Корень бинарного дерева поиска.</param>
    /// <param name="key">Ключ удаляемого узла.</param>
    /// <returns>Корень измененного бинарного дерева поиска.</returns>
    public static Node Remove(Node root, int key)
    {
        if (root == null)
        {
            return root;
        }

        if (key < root.Value)
        {
            root.Left = Remove(root.Left, key);
        }

        else if (key > root.Value)
        {
            root.Right = Remove(root.Right, key);
        }

        else
        {
            if (root.Left == null)
            {
                return root.Right;
            }
            else if (root.Right == null)
            {
                return root.Left;
            }

            root.Value = MinValue(root.Right);
            root.Right = Remove(root.Right, root.Value);
        }
        return root;
    }
    /// <summary>
    /// Находит наименьшее значение в бинарном дереве поиска.
    /// </summary>
    /// <param name="node">Корень бинарного дерева поиска.</param>
    /// <returns>Наименьшее значение в бинарном дереве поиска.</returns>
    private static int MinValue(Node node)
    {
        int minValue = node.Value;

        while (node.Left != null)
        {
            minValue = node.Left.Value;
            node = node.Left;
        }

        return minValue;
    }
    /*
    public class Node
    {
        public int Value { get; set; }
        public Node Left { get; set; }
        public Node Right { get; set; }

        public Node(int value)
        {
            Value = value;
            Left = null;
            Right = null;
        }
    }
    */
    /*
    public static void Test()
    {
        var node1 = new Node(2);
        var node2 = new Node(3)
        {
            Left = node1
        };

        var node3 = new Node(1)
        {
            Right = node2
        };

        var node4 = new Node(6);
        var node5 = new Node(8)
        {
            Left = node4
        };

        var node6 = new Node(10)
        {
            Left = node5
        };

        var node7 = new Node(5)
        {
            Left = node3,
            Right = node6
        };

        var newHead = Remove(node7, 10);

        Console.WriteLine(newHead.Value == 5);
        Console.WriteLine(newHead.Right == node5);
        Console.WriteLine(newHead.Right.Value == 8);
    }
    */
}
