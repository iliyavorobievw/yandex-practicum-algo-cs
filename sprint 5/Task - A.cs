﻿using System;

public class Solution
{
    public static int Solve(Node root)
    {
        if (root == null) return int.MinValue;
        return Math.Max(root.Value, Math.Max(Solve(root.Left), Solve(root.Right)));
    }
}
