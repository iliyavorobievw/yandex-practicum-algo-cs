using System;
using System.Collections.Generic;
using System.Numerics;
using System.Text;

class Program
{
    static void Main()
    {
        var n = int.Parse(Console.ReadLine());
        var dp = new long[n + 1];
        dp[0] = 1;
        dp[1] = 1;

        for (var i = 2; i <= n; i++)
        {
            for (var j = 0; j < i; j++)
            {
                dp[i] += dp[j] * dp[i - j - 1];
            }
        }

        Console.WriteLine(dp[n]);
    }
}
