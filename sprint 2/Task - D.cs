﻿using System;
using System.Linq;
using System.Collections;
using System.Collections.Generic;
using System.Security.Cryptography.X509Certificates;
using System.Globalization;
using System.Reflection;

public class Solution
{
    public static int Solve(Node<string> head, string item)
    {
        var i = 0;
        while (head != null)
        {
            if (head.Value == item)
            {
                return i;
            }
            i++;
            head = head.Next;
        }
        return -1;
    }

    private static void Test()
    {
        var node3 = new Node<string>("node3", null);
        var node2 = new Node<string>("node2", node3);
        var node1 = new Node<string>("node1", node2);
        var node0 = new Node<string>("node0", node1);
        var idx = Solution.Solve(node0, "node2");
        // result is : idx == 2
    }
}
