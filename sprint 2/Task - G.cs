using System;
using System.Linq;
using System.Collections;
using System.Collections.Generic;
using System.Security.Cryptography.X509Certificates;
using System.Globalization;

public class Program
{

    static void Main(string[] args)
    {
        var commandCount = int.Parse(Console.ReadLine());

        var newStack = new StackMax();

        for (int i = 0; i < commandCount; i++)
        {
            var currentCommand = Console.ReadLine();
            if (currentCommand.Contains("get"))
            {
                newStack.GetMax();
            }
            if (currentCommand.Contains("pop"))
            {
                newStack.Pop();
            }
            if (currentCommand.Contains("push"))
            {

                newStack.Push(int.Parse(currentCommand.Split(" ")[1]));
            }

        }



    }
}

public class StackMax
{
    List<int> stackList = new List<int>();
    List<int> listMaxElement = new List<int>() { int.MinValue };

    // Добавить x
    public void Push(int x)
    {
        if (stackList.Count == 0)
        {
            listMaxElement.Add(x);
            stackList.Add(x);
            return;
        }

        stackList.Add(x);

        var max = listMaxElement.Last() < x ? x : listMaxElement.Last();

        listMaxElement.Add(max);
    }

    // Убрать lastElement
    public void Pop()
    {
        if (stackList.Count == 0)
        {
            Console.WriteLine("error");
            return;
        }
        var lastElementIndex = stackList.Count - 1;
        stackList.RemoveAt(lastElementIndex);

        listMaxElement.RemoveAt(listMaxElement.Count - 1);

    }
    public void GetMax()
    {
        if (stackList.Count == 0)
        {
            Console.WriteLine("None");
            return;
        }
        Console.WriteLine(listMaxElement.Last());

    }
}