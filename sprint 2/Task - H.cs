using System;
using System.Linq;
using System.Collections;
using System.Collections.Generic;
using System.Security.Cryptography.X509Certificates;
using System.Globalization;

public class Program
{

    static void Main(string[] args)
    {
        var str = Console.ReadLine();
        var answer = IsCorrectBracketSeq(str);
        Console.WriteLine(answer);


    }

    static bool IsCorrectBracketSeq(string str)
    {
        var open = new List<char> { '[', '(', '{' };
        var close = new List<char> { ']', ')', '}' };

        var stack = new Stack<char>();

        foreach (var item in str) 
        {
            if (open.Contains(item))
            {
                stack.Push(item);
            }
            if (close.Contains(item))
            {
                if (stack.Count == 0)
                {
                    return false;
                }
                
                var index = open.IndexOf(stack.Pop());
                if (close[index] != item)
                {
                    return false;
                }
            }
        }
        return stack.Count == 0;
    }
}
