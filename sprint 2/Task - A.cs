using System;
using System.Linq;
using System.Collections;
using System.Collections.Generic;
using System.Security.Cryptography.X509Certificates;
using System.Globalization;

public class Program
{

    static void Main(string[] args)
    {
        var n = int.Parse(Console.ReadLine());
        var m = int.Parse(Console.ReadLine());
        if (n == 0 || m == 0)
        {
            return;
        }

        int[][] sourceMatrix = new int[n][];

        for (int i = 0; i < n; i++)
        {
            sourceMatrix[i] = new int[m];
            var strM = Console.ReadLine().Split(' ');
            for (int j = 0; j < m; j++)
            {
                sourceMatrix[i][j] = int.Parse(strM[j]);
            }

        }
        var transponceMatrix = matrixTransponce(sourceMatrix);
        for (int i = 0; i < transponceMatrix.Length; i++)
        {
            
            for (int j = 0; j < transponceMatrix[0].Length; j++)
            {
                Console.Write(transponceMatrix[i][j]+ " ");
            }
            Console.WriteLine();
        }

    }

    static int[][] matrixTransponce(int[][] sourceMatrix)
    {
        var newMatrix = new int[sourceMatrix[0].Length][];

        for (int i = 0; i < sourceMatrix[0].Length; i++)
        {
            newMatrix[i] = new int[sourceMatrix.Length];
        }

        for (int i = 0; i < sourceMatrix.Length; i++)
        {
            for (int j = 0; j < sourceMatrix[i].Length; j++)
            {
                newMatrix[j][i] = sourceMatrix[i][j];
            }
        }

        return newMatrix;
    }

}