using System;
using System.Linq;
using System.Collections;
using System.Collections.Generic;
using System.Security.Cryptography.X509Certificates;
using System.Globalization;
using System.Runtime.Intrinsics.X86;

public class Program
{
    static void Main(string[] args)
    {

        var commandCount = int.Parse(Console.ReadLine());
        LinkedListManager linkedListManager = new LinkedListManager();

        for (int i = 0; i < commandCount; i++)
        {
            var command = Console.ReadLine();
            if (command.Contains("get"))
            {
                linkedListManager.Get();

            }
            if (command.Contains("size"))
            {
                linkedListManager.Size();
            }
            if (command.Contains("put"))
            {

                linkedListManager.Put(int.Parse(command.Split(' ')[1]));
            }
        }


        {
            Node first = new Node(1);
            Node second = new Node(2);
            first.Next = second;
            Node third = new Node(3);
            second.Next = third;
        }
        void Print(Node head)
        {
            while (head != null)
            {
                Console.WriteLine(head.Value);
                head = head.Next;
            }
        }

        void FindByIndex(Node head, int index)
        {
            if (index < 0)
            {
                Console.WriteLine("This element don`t exists");
                return;
            }
            var i = 0;
            while (head != null)
            {
                if (i == index)
                {
                    Console.WriteLine(head.Value);
                    return;
                }

                head = head.Next;
                i++;
            }
            Console.WriteLine("This element don`t exists");
        }

        void FindByValue(Node head, int value)
        {
            var i = 0;
            while (head != null)
            {
                if (head.Value == value)
                {
                    Console.WriteLine(i);
                    return;
                }
                i++;
                head = head.Next;
            }
            Console.WriteLine("-1");
        }

        Node DeleteByValue(Node head, int value)
        {
            Node FirstHead = head;
            Node previous = null;

            while (head != null)
            {
                if (head.Value == value)
                {
                    if (previous == null)
                    {
                        var newHead = head.Next;
                        head.Next = null;
                        return newHead;
                    }

                    previous.Next = head.Next;
                    head.Next = null;
                    return FirstHead;
                }
                previous = head;
                head = head.Next;
            }

            return FirstHead;
        }

    }
}
public class Node
{
    public Node Next;
    public int Value;

    public Node(int value)
    {
        Next = null;
        Value = value;
    }
}

public class LinkedListManager
{
    Node head;
    Node tail;

    int size;

    public void Put(int value)
    {
        if (head == null)
        {
            head = new Node(value: value);
            tail = head;
            size++;
            return;
        }

        tail.Next = new Node(value: value);
        tail = tail.Next;
        size++;
    }

    public void Get()
    {
        if (head == null)
        {
            Console.WriteLine("error");
            return;
        }

        Console.WriteLine(head.Value);

        var next = head.Next;
        head.Next = null;
        head = next;

        size--;
    }

    public void Size()
    {
        Console.WriteLine(size);
    }
}