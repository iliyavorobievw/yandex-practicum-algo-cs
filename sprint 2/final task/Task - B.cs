﻿using System;
using System.Linq;
using System.Collections;
using System.Collections.Generic;
using System.Security.Cryptography.X509Certificates;
using System.Globalization;
using System.Runtime.Intrinsics.X86;

public class Program
{
    // report link - https://contest.yandex.ru/contest/22781/run-report/87836541/

    // Я реализовал калькулятор при помощи стека, алгоритм работает записывая числа в стек, пока не найдёт знак операнда,
    // после нахождения знака, он вытаскивает числа из стека и производит операцию над числами, помещая результат операции назад в стек
    // Временная сложность моего алгоритма O(n) - Из-за того что итерируюсь по строке используя цикл foreach
    // Пространственная сложность (сложность по памяти) O(n) Из-за того, что я храню элементы строки.

    static void Main(string[] args)
    {
        var operands = new Dictionary<string, Func<int, int, int>>{
            {"*", Multiply },
            {"+", Sum },
            { "-", Subtraction},
            {"/", Divide } };

        var str = Console.ReadLine();

        var arraySplitString = str.Split(' ');

        var numberStack = new Stack<int>();

        foreach (var item in arraySplitString)
        { 
            // знак или число
            if (!operands.Keys.Contains(item))
            {
                numberStack.Push(int.Parse(item));
                continue;
            }
            // Достать и удалить числа из стека
            var secondNumber = numberStack.Pop();
            var firstNumber = numberStack.Pop();

            var Delegate = operands[item];
            var result = Delegate(firstNumber, secondNumber);

            numberStack.Push(result);
        }
        Console.WriteLine(numberStack.Pop());

    }

    static int Sum(int firstNum, int secondNumber)
    {
        return firstNum + secondNumber;
    }
    static int Divide(int firstNum, int secondNumber)
    {
        return (int)(Math.Floor(firstNum * 1.0 / secondNumber));
    }
    static int Multiply(int firstNum, int secondNumber)
    {
        return firstNum * secondNumber;
    }
    static int Subtraction(int firstNum, int secondNumber)
    {
        return firstNum - secondNumber;
    }
}
