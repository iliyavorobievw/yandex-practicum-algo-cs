﻿using System;
using System.Linq;
using System.Collections;
using System.Collections.Generic;
using System.Security.Cryptography.X509Certificates;
using System.Globalization;
using System.Runtime.Intrinsics.X86;

public class Program
{
    /*
     * report link - https://contest.yandex.ru/contest/22781/run-report/87835949/
     * 
     * Принцип работы моего алгоритма - кольцевая очередь.
     * В процессе выполнения я храню head, tail и size.
     * Size - отслеживать количество элементов,
     * Tail - отслеживать конец очереди,
     * Head - отслеживать начало очереди.
     * В процессе работы с очередью сдвигаю tail и head.
     * 
     * Временная сложность этого алгоритма O(1), тк я не прохожусь по массиву,
     * и количество операций не изменяется от увеличения данных
	 * Затраты по памяти O(n) тк я храню массив
     */
    static void Main(string[] args)
    {

        var commandCount = int.Parse(Console.ReadLine());
        var maxSizeQueue = int.Parse(Console.ReadLine());

        var myDeck = new Deck(maxSizeQueue);


        for (int i = 0; i < commandCount; i++)
        {
            var command = Console.ReadLine();
            if (command.Contains("push_back"))
            {
                myDeck.PushBack(int.Parse(command.Split(' ')[1]));
            }
            if (command.Contains("push_front"))
            {
                myDeck.PushFront(int.Parse(command.Split(' ')[1]));
            }
            if (command.Contains("pop_back"))
            {
               myDeck.PopBack();
            }
            if (command.Contains("pop_front"))
            {

                myDeck.PopFront();
            }
        }
    }
}
/// <summary>
/// Класс реализующий работу структуры данных - Дэк
/// Очередь с двумя концами, работающая по схеме кольцевой очереди
/// </summary>
class Deck
{
    /// <summary>
    /// Массив чисел, нужный, для хранения значений
    /// </summary>
    int[] myQueue;

    /// <summary>
    /// Максимальный размер дека
    /// </summary>
    int _maxSize;

    /// <summary>
    /// Индекс головы
    /// </summary>
    int head;

    /// <summary>
    /// Индекс Хвоста
    /// </summary>
    int tail;

    /// <summary>
    /// Текущий размер дека
    /// </summary>
    int size;

    /// <summary>
    /// сообщение об ошибке
    /// </summary>
    const string errorMessage = "error";


    /// <summary>
    /// Конструктор ctor
    /// </summary>
    /// <param name="maxSize">Максимальный размер дека</param>
    public Deck(int maxSize)
    {
        _maxSize = maxSize;

        myQueue = new int[_maxSize];

        head = 0;
        tail = 0;
        size = 0;
    }
    public bool IsEmpty()
    {
        return size == 0;
    }

    /// <summary>
    /// Вставка в конец
    /// </summary>
    /// <param name="element">элемент, который вставляем</param>
    public void PushBack(int element)
    {
        if (IsFull())
        {
            Console.WriteLine(errorMessage);
            return;
        }
        myQueue[tail] = element;
        tail = (tail + 1) % _maxSize;
        size++;
    }

    /// <summary>
    /// Вставка в начало
    /// </summary>
    /// <param name="element"></param>
    public void PushFront(int element)
    {
        if (IsFull())
        {
            Console.WriteLine(errorMessage);
            return;
        }
        head--;
        if (head < 0)
        {
            head = myQueue.Length - 1;
        }
        myQueue[head] = element;
        size++;
    }

    /// <summary>
    /// Удаление с начала (удалить первый элемент)
    /// </summary>
    public void PopFront()
    
    {
        if (IsEmpty())
        {
            Console.WriteLine(errorMessage);
            return;
        }
        Console.WriteLine(myQueue[head]);
        var headElement = myQueue[head];
        head = (head + 1) % _maxSize;
        size--;
        return;
    }

    /// <summary>
    /// Удалить с конца (удалить последний)
    /// </summary>
    public void PopBack()
    {
        if (IsEmpty())
        {
            Console.WriteLine(errorMessage);
            return;
        }

        tail--;
        if (tail < 0)
        {
            tail = myQueue.Length - 1;
        }
        Console.WriteLine(myQueue[tail]);

       
        size--;
        return;
    }


    public bool IsFull()
    {
        return size >= _maxSize;
    }

}