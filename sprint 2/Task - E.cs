using System;
using System.Linq;
using System.Collections;
using System.Collections.Generic;
using System.Security.Cryptography.X509Certificates;
using System.Globalization;

public class Program
{

    static void Main(string[] args)
    {
        var commandCount = int.Parse(Console.ReadLine());

        var newStack = new StackMax();

        for (int i = 0; i < commandCount; i++)
        {
            var currentCommand = Console.ReadLine();
            if (currentCommand.Contains("get"))
            {
                newStack.GetMax();
            }
            if (currentCommand.Contains("pop"))
            {
                newStack.Pop();
            }
            if (currentCommand.Contains("push"))
            {

                newStack.Push(int.Parse(currentCommand.Split(" ")[1]));
            }

        }



    }
}

public class StackMax
{
    List<int> stackList = new List<int>();

    public void Push(int x)
    {
        stackList.Add(x);
    }
    public void Pop()
    {
        if (stackList.Count == 0)
        {
            Console.WriteLine("error");
            return;
        }
        var lastElementIndex = stackList.Count - 1;
        stackList.RemoveAt(lastElementIndex);
    }
    public void GetMax()
    {
        if (stackList.Count == 0)
        {
            Console.WriteLine("None");
            return;
        }
        Console.WriteLine(stackList.Max().ToString());

        
    }
}