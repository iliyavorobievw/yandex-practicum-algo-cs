using System;
using System.Linq;
using System.Collections;
using System.Collections.Generic;
using System.Security.Cryptography.X509Certificates;
using System.Globalization;
using System.Runtime.Intrinsics.X86;

public class Program
{
    static void Main(string[] args)
    {
        var commandCount = int.Parse(Console.ReadLine());
        var maxSizeQueue = int.Parse(Console.ReadLine());

        var customQueue = new MyQueueSized(maxSizeQueue);

        for (int i = 0; i < commandCount; i++)
        {
            var command = Console.ReadLine();
            if (command.Contains("peek"))
            {
                customQueue.Peek();
            }
            if (command.Contains("size"))
            {
                customQueue.Size();
            }
            if (command.Contains("push"))
            {
                customQueue.Push(int.Parse(command.Split(' ')[1]));

            }
            if (command.Contains("pop"))
            {
                customQueue.Pop();
            }
        }

    }
}
class MyQueueSized
{
    int?[] myQueue;

    int _maxSize;

    // pop index
    int head;

    // push index
    int tail;

    // current size queue
    int size;

    public MyQueueSized(int maxSize)
    {
        _maxSize = maxSize;

        myQueue = new int?[_maxSize];

        head = 0;
        tail = 0;
        size = 0;
    }
    public bool IsEmpty()
    {
        return size == 0;
    }
    public void Push(int element)
    {
        if (size >= _maxSize)
        {
            Console.WriteLine("error");
            return;
        }
        myQueue[tail] = element;
        tail = (tail + 1) % _maxSize;
        size++;
    }
    public void Pop()
    {
        if (IsEmpty())
        {
            Console.WriteLine("None");
            return;
        }
        var headElement = myQueue[head];
        myQueue[head] = null;
        head = (head + 1) % _maxSize;
        size--;
        Console.WriteLine($"{headElement}");
        return;
    }
    public void Peek()
    {
        if (IsEmpty())
        {
            Console.WriteLine("None");
            return;
        }

        Console.WriteLine($"{myQueue[head]}");
    }
    public void Size()
    {
        Console.WriteLine(size);
    }
}