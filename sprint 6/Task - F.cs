using System;
using System.Collections.Generic;
using System.Linq;

class Program
{
    static void Main(string[] args)
    {
        var input = Console.ReadLine().Split(' ').Select(int.Parse).ToArray();
        int n = input[0];
        int m = input[1];

        var adjacencyList = new List<int>[n];
        for (int i = 0; i < n; i++)
        {
            adjacencyList[i] = new List<int>();
        }

        for (int i = 0; i < m; i++)
        {
            var edge = Console.ReadLine().Split(' ').Select(int.Parse).ToArray();
            int u = edge[0] - 1;
            int v = edge[1] - 1;

            adjacencyList[u].Add(v);
            adjacencyList[v].Add(u);
        }

        var st = Console.ReadLine().Split(' ').Select(int.Parse).ToArray();
        int s = st[0] - 1;
        int t = st[1] - 1;

        var distances = new int[n];
        for (int i = 0; i < n; i++)
        {
            distances[i] = -1;
        }
        
        var queue = new Queue<int>();
        queue.Enqueue(s);
        distances[s] = 0;

        while (queue.Count > 0)
        {
            int v = queue.Dequeue();

            foreach (var u in adjacencyList[v])
            {
                if (distances[u] == -1)
                {
                    queue.Enqueue(u);
                    distances[u] = distances[v] + 1;
                }
            }
        }

        Console.WriteLine(distances[t]);
    }
}