using System;
using System.Collections.Generic;
/*
 * успешная посылка - https://contest.yandex.ru/contest/25070/run-report/92028971/
 * 
 *  Временная сложность:
    O(V+E) - как в DFS со списками смежности.
    Пространственная сложность:
    O(V+E), где E - количество рёбер.
	список смежности графа требует O(V+E) памяти,
	а массив цветов требует O(V) памяти.
	
	ответ на вопрос после ревью - Да, массив для хранения цветов будет включен в формулу для пространственной сложности. 
	Он используеся для  отслеживания состояния вершин в графе и имеет размер O(V), где V - количество вершин.
	
 * Код реализует алгоритм поиска в глубину для определения наличия циклов в графе.
 * Он считывает граф из стандартного ввода и проверяет его на наличие циклов.
 * Если в графе есть циклы, выводится “NO” или “YES”.
 * 
 * ПРИНЦИП РАБОТЫ
    Получим граф - если для одного из типов дороги поменять направление ребра.
    Для каждой вершины запускаем обход DFS, если граф имеет цикл, значит путь неоптимальный.


    С помощью метода DfsIsCyclic() определяем оптимальность карты железных дорог.
    создаем отдельный "цветовой" массив, где будем помечать вершины тремя цветами:

    - белый - не посещенный город,
    - серый - уже посещенный, но не все его ребра обработаны,
    - черный - город уже посещен и все его ребра обработаны.

    Таким образом, если в процессе обхода графа мы наткнемся на серый город, это означает, что в графе есть цикл.
    Значит существует пара городов, между которыми есть маршрут с разным типом дорог и
    карта железных дорог в этом случае является не оптимальной.
 */

public class Program
{
    // Enum - Перечисление для обозначения цветов вершин в графе
    private enum Color
    {
        White,
        Gray,
        Black
    }

    // Сообщение об ошибке
    const string errorMessage = "Unknown road type";

    // Константы для обозначения типов дорог
    private const char wideRoad = 'B';
    private const char narrowRoad = 'R';

    public static void Main()
    {
        // Чтение количества вершин
        int n = int.Parse(Console.ReadLine().Trim());
        var adjacency = new Dictionary<int, List<int>>();

        // Инициализация графа
        for (int i = 0; i < n; i++)
        {
            adjacency[i] = new List<int>();
        }

        // Чтение типов дорог и заполнение графа
        for (int i = 0; i < n - 1; i++)
        {
            string roadTypes = Console.ReadLine().Trim();
            for (int j = 0; j < roadTypes.Length; j++)
            {
                char typeRoad = roadTypes[j];
                if (typeRoad == wideRoad)
                {
                    adjacency[i].Add(i + j + 1);
                }
                else if (typeRoad == narrowRoad)
                {
                    adjacency[i + j + 1].Add(i);
                }
                else
                {
                    throw new Exception(errorMessage);
                }
            }
        }

        // Проверка графа на наличие циклов
        Console.WriteLine(IsCyclic(adjacency) ? "NO" : "YES");
    }

    /// <summary>
    /// Проверяет, содержит ли граф циклы.
    /// </summary>
    /// <param name="listAdjacencyGraph">Список смежности графа.</param>
    /// <returns>Возвращает true, если граф содержит циклы, иначе false.</returns>
    private static bool IsCyclic(Dictionary<int, List<int>> listAdjacencyGraph)
    {
        var colors = new Color[listAdjacencyGraph.Count];
        for (int i = 0; i < listAdjacencyGraph.Count; i++)
        {
            if (DfsIsCyclic(i, listAdjacencyGraph, colors))
            {
                return true;
            }
        }

        return false;
    }

    /// <summary>
    /// Выполняет обход в глубину для проверки наличия циклов.
    /// </summary>
    /// <param name="startVertex">Начальная вершина для обхода.</param>
    /// <param name="listAdjacencyGraph">Список смежности графа.</param>
    /// <param name="colors">Массив цветов вершин.</param>
    /// <returns>Возвращает true, если найден цикл, иначе false.</returns>
    private static bool DfsIsCyclic(int startVertex, Dictionary<int, List<int>> listAdjacencyGraph, Color[] colors)
    {
        var stack = new Stack<int>();
        stack.Push(startVertex);

        while (stack.Count > 0)
        {
            int v = stack.Pop();
            if (colors[v] == Color.White)
            {
                colors[v] = Color.Gray;
                stack.Push(v);

                foreach (int w in listAdjacencyGraph[v])
                {
                    if (colors[w] == Color.White)
                    {
                        stack.Push(w);
                    }
                    else if (colors[w] == Color.Gray)
                    {
                        return true;
                    }
                }
            }
            else if (colors[v] == Color.Gray)
            {
                colors[v] = Color.Black;
            }
        }

        return false;
    }
}
