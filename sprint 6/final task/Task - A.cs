using System;
using System.Collections.Generic;

/*
SIX SPRINT - final task - A
успешная посылка - https://contest.yandex.ru/contest/25070/run-report/92026875/

Этот код реализует алгоритм Прима
для построения максимального остовного дерева.
Он считывает граф из стандартного ввода,
строит максимальное остовное дерево и выводит его вес.
Если граф не связный и невозможно построить остовное дерево,
выводится сообщение об ошибке errorMessage


ВРЕМЕННАЯ СЛОЖНОСТЬ
    O(E*logV), E - количество рёбер в графе, V - количество вершин.

    Используем - приоритетную очередь.
    При поиске минимального (максимального элемента) мы затрачиваем О(logV) времени, V - количество вершин в графе.
    Если вместе с ребром в подграф добавляется новая вершина, то это ребро добавляется в остов.
    Если ребро соединяет две вершины, уже присутствующее в подмножестве остова,
    мы отбрасываем его из дальнейшего рассмотрения.
    В общем случае временная сложность алгоритма получается O(E*logV), где E - количество ребер графа.

ПРОСТРАНСТВЕННАЯ СЛОЖНОСТЬ
    Хранение кучи - O(n).
    Список смежности - O(E*V), E - количество вершин, V - количество рёбер.

*/
public class Program
{
    public static void Main()
    {
        const string errorMessage =  "Oops! I did it again";

        // Чтение количества вершин и ребер
        var input = Console.ReadLine().Split();
        int n = int.Parse(input[0]);
        int m = int.Parse(input[1]);

        // Инициализация графа
        var graph = new List<(int, int)>[n + 1];
        for (int i = 0; i <= n; i++)
        {
            graph[i] = new List<(int, int)>();
        }

        // Чтение ребер графа
        for (int i = 0; i < m; i++)
        {
            input = Console.ReadLine().Split();
            int f = int.Parse(input[0]);
            int t = int.Parse(input[1]);
            int w = int.Parse(input[2]);

            graph[f].Add((t, w));
            graph[t].Add((f, w));
        }

        // Инициализация массива посещенных вершин и очереди с приоритетами для ребер
        var added = new bool[n + 1];
        var edges = new SortedSet<(int, int)>();
        int maximumSpanningTree = 0;

        added[0] = true;
        AddVertex(1, graph[1], added, edges);

        // Построение максимального остовного дерева
        while (Array.IndexOf(added, false) != -1 && edges.Count > 0)
        {
            var edge = edges.Min;
            edges.Remove(edge);

            if (!added[edge.Item2])
            {
                maximumSpanningTree += Math.Abs(edge.Item1);
                AddVertex(edge.Item2, graph[edge.Item2], added, edges);
            }
        }

        // Вывод результата
        Console.WriteLine(Array.IndexOf(added, false) != -1 ? errorMessage : maximumSpanningTree.ToString());
    }

    /// <summary>
    /// Добавляет вершину в очередь с приоритетами.
    /// </summary>
    /// <param name="vertex">Вершина для добавления.</param>
    /// <param name="graphEdges">Список ребер графа.</param>
    /// <param name="added">Массив посещенных вершин.</param>
    /// <param name="edges">Очередь с приоритетами для ребер.</param>
    private static void AddVertex(int vertex, List<(int, int)> graphEdges, bool[] added, SortedSet<(int, int)> edges)
    {
        added[vertex] = true;

        foreach (var edge in graphEdges)
        {
            if (!added[edge.Item1])
            {
                edges.Add((-edge.Item2, edge.Item1));
            }
        }
    }
}
