using System;
using System.Collections.Generic;
using System.Linq;

class Program
{
    static void Main(string[] args)
    {
        var input = Console.ReadLine().Split(' ').Select(int.Parse).ToArray();
        int n = input[0];
        int m = input[1];

        var adjacencyList = new List<int>[n];
        for (int i = 0; i < n; i++)
        {
            adjacencyList[i] = new List<int>();
        }

        for (int i = 0; i < m; i++)
        {
            var edge = Console.ReadLine().Split(' ').Select(int.Parse).ToArray();
            int u = edge[0] - 1;
            int v = edge[1] - 1;

            adjacencyList[u].Add(v);
            adjacencyList[v].Add(u);
        }

        int s = int.Parse(Console.ReadLine()) - 1;

        var visited = new bool[n];
        DFS(s, adjacencyList, visited);
    }

    static void DFS(int v, List<int>[] adjacencyList, bool[] visited)
    {
        visited[v] = true;
        Console.Write((v + 1) + " ");

        foreach (var u in adjacencyList[v].OrderBy(x => x))
        {
            if (!visited[u])
            {
                DFS(u, adjacencyList, visited);
            }
        }
    }
}