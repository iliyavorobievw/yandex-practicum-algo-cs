using System;
using System.Collections.Generic;
using System.Linq;

class Program
{
    static void Main(string[] args)
    {
        var input = Console.ReadLine().Split(' ').Select(int.Parse).ToArray();
        int n = input[0];
        int m = input[1];

        var adjacencyList = new List<int>[n];
        for (int i = 0; i < n; i++)
        {
            adjacencyList[i] = new List<int>();
        }

        for (int i = 0; i < m; i++)
        {
            var edge = Console.ReadLine().Split(' ').Select(int.Parse).ToArray();
            int u = edge[0] - 1;
            int v = edge[1] - 1;

            adjacencyList[u].Add(v);
        }

        for (int i = 0; i < n; i++)
        {
            adjacencyList[i].Sort();
            Console.WriteLine(adjacencyList[i].Count + " " + string.Join(" ", adjacencyList[i].Select(x => x + 1)));
        }
    }
}