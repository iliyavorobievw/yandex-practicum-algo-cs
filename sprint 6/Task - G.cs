using System;
using System.Collections.Generic;

namespace Graph
{
    class Program
    {
        static void Main(string[] args)
        {
            string[] input = Console.ReadLine().Split();
            int n = int.Parse(input[0]);
            int m = int.Parse(input[1]);

            List<int>[] graph = new List<int>[n];
            for (int i = 0; i < n; i++)
                graph[i] = new List<int>();

            for (int i = 0; i < m; i++)
            {
                input = Console.ReadLine().Split();
                int u = int.Parse(input[0]) - 1;
                int v = int.Parse(input[1]) - 1;

                graph[u].Add(v);
                graph[v].Add(u);
            }

            int s = int.Parse(Console.ReadLine()) - 1;

            Queue<int> queue = new Queue<int>();
            queue.Enqueue(s);

            int[] dist = new int[n];
            for (int i = 0; i < n; i++)
                dist[i] = -1;
            dist[s] = 0;

            while (queue.Count > 0)
            {
                int u = queue.Dequeue();

                foreach (int v in graph[u])
                {
                    if (dist[v] == -1)
                    {
                        dist[v] = dist[u] + 1;
                        queue.Enqueue(v);
                    }
                }
            }

            int maxDist = 0;
            for (int i = 0; i < n; i++)
                maxDist = Math.Max(maxDist, dist[i]);

            Console.WriteLine(maxDist);
        }
    }
}
