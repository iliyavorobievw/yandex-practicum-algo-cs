using System;
using System.Collections.Generic;
using System.Linq;

namespace DepthFirstSearch
{
    class Program
    {
        static int time = 0;
        static int[] tin, tout;
        static List<int>[] adjList;

        static void DFS(int v)
        {
            tin[v] = time++;
            foreach (int u in adjList[v])
                if (tin[u] == -1)
                    DFS(u);
            tout[v] = time++;
        }

        static void Main(string[] args)
        {
            var input = Console.ReadLine().Split(' ').Select(int.Parse).ToArray();
            int n = input[0];
            int m = input[1];

            adjList = new List<int>[n];
            for (int i = 0; i < n; i++)
                adjList[i] = new List<int>();

            for (int i = 0; i < m; i++)
            {
                input = Console.ReadLine().Split(' ').Select(int.Parse).ToArray();
                int u = input[0] - 1;
                int v = input[1] - 1;
                adjList[u].Add(v);
            }

            for (int i = 0; i < n; i++)
                adjList[i].Sort();

            tin = Enumerable.Repeat(-1, n).ToArray();
            tout = Enumerable.Repeat(-1, n).ToArray();

            DFS(0);

            for (int i = 0; i < n; i++)
                Console.WriteLine(tin[i] + " " + tout[i]);
        }
    }
}
