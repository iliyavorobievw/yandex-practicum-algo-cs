using System;
using System.Linq;
using System.Collections;
using System.Collections.Generic;
using System.Security.Cryptography.X509Certificates;
using System.Globalization;
using System.Runtime.Intrinsics.X86;

public class Program
{
    static void Main(string[] args)
    {

        var allCount = Console.ReadLine();
        var array = Console.ReadLine().Split(' ');
        var topCount = int.Parse(Console.ReadLine());

        var dict = new Dictionary<string, int>();

        foreach (var i in array)
        {
            if (!dict.ContainsKey(i))
            {
                dict.Add(i, 0);
            }
            dict[i]++;
        }

        var ordered = dict.OrderByDescending(x => x.Value)
            .Take(topCount)
            .Select(x=>x.Key);
        Console.WriteLine(string.Join(' ',ordered));
    }
}
