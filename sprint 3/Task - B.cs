using System;
using System.Collections.Generic;

namespace ConsoleApp1
{
    class Program
    {
        static void Main(string[] args)
        {
            string vvod = Console.ReadLine();
            letterCombinations(vvod);
        }

        static void letterCombinations(string digits)
        {
            Dictionary<char, string> letters = new Dictionary<char, string>()
            {
                {'2', "abc"},
                {'3', "def"},
                {'4', "ghi"},
                {'5', "jkl"},
                {'6', "mno"},
                {'7', "pqrs"},
                {'8', "tuv"},
                {'9', "wxyz"}
            };

            List<string> res = new List<string>();
            backtrack(digits, "", res, letters);

            foreach (string x in res)
            {
                Console.Write(x + " ");
            }
        }

        static void backtrack(string digits, string path, List<string> res, Dictionary<char, string> letters)
        {
            if (digits == "")
            {
                res.Add(path);
                return;
            }

            foreach (char letter in letters[digits[0]])
            {
                path += letter;
                backtrack(digits.Substring(1), path, res, letters);
                path = path.Remove(path.Length - 1);
            }
        }
    }
}