using System;

class Program
{
    static void Main(string[] args)
    {
        int countNumbers = int.Parse(Console.ReadLine());
        string[] digits = Console.ReadLine().Split(' ');

        for (int i = 0; i < countNumbers - 1; i++)
        {
            for (int j = 0; j < countNumbers - i - 1; j++)
            {
                string var1 = digits[j] + digits[j + 1];
                string var2 = digits[j + 1] + digits[j];
                if (var1.CompareTo(var2) < 0)
                {
                    string temp = digits[j];
                    digits[j] = digits[j + 1];
                    digits[j + 1] = temp;
                }
            }
        }

        Console.WriteLine(string.Join("", digits));
    }
}
