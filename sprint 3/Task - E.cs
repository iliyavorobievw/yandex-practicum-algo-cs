using System;
using System.IO;
using System.Linq;

class Program
{
    static void Main(string[] args)
    {
        string[] arrayInput = Console.ReadLine()
                                      .Split();
        int num = int.Parse(arrayInput[0]);
        int budget = int.Parse(arrayInput[1]);
        int[] housePriceArray = Console.ReadLine()
            .Split()
            .Select(int.Parse)
            .ToArray();
        Console.WriteLine(GetCountHouse(budget, housePriceArray));
    }

    static int GetCountHouse(int budget, int[] cost)
    {
        if (cost.Length > 1)
        {
            cost = cost.OrderBy(x => x)
                        .ToArray();
        }

        int count = 0;

        foreach (int house in cost)
        {
            budget -= house;

            if (budget >= 0)
            {
                count++;
            }
            else
            {
                return count;
            }
        }

        return count;
    }
}
