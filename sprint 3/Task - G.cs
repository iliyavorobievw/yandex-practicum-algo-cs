using System;
using System.IO;
using System.Linq;

class Program
{
    static void Main(string[] args)
    {
        if (int.Parse(Console.ReadLine()) == 0)
        {
            return;
        }
        
        Console.WriteLine(String.Join(" ", Console.ReadLine().Split(' ')
            .Select(x => int.Parse(x))
            .OrderBy(x => x)));
    }
}
