using System;
using System.IO;
using System.Linq;

class Program
{
    static void Main(string[] args)
    {
        int n = int.Parse(Console.ReadLine());
        int[] arraySideLength = Console.ReadLine()
                          .Split()
                          .Select(int.Parse)
                          .OrderByDescending(x => x).ToArray();

        for (int i = 0; i < arraySideLength.Length - 2; i++)
        {
            if (arraySideLength[i] < arraySideLength[i + 1] + arraySideLength[i + 2])
            {
                Console.WriteLine(arraySideLength[i] + arraySideLength[i + 1] + arraySideLength[i + 2]);
                break;
            }
        }
    }
}
