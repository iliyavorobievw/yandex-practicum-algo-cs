using System;
using System.Collections.Generic;

namespace QuickSort
{
    class Program
    {
        /// <summary>
        /// 
        /// Report Link - https://contest.yandex.ru/contest/23815/run-report/87948509/
        /// 
        /// Временная сложность этого алгоритма в среднем составляет O(n log n),
        /// Где N - размер листа участника
        /// В худшем случае временная сложность может достигать O(n^2), как и у всех алгоритмов сортировок,
        /// Кроме : Сортировки слияением, Пирамидальной сортировки и поразрядной сортировки
        /// 
        /// Пространственная сложность (Затраты по памяти) этого алгоритма составляет O(log n),
        /// так как он использует рекурсию и хранит стек вызовов рекурсии.
        /// 
        /// Быстрая сортировка (в сигнатуре метода используются значения по умолчанию int start = 0, int end = 0)
        /// Как работает InPlaceQuickSort
        /// Как и в случае обычной быстрой сортировки, которая использует дополнительную память, необходимо выбрать опорный элемент (англ.pivot),
        /// а затем переупорядочить массив.Сделаем так, чтобы сначала шли элементы, не превосходящие опорного, а затем —– большие опорного.
        /// Затем сортировка вызывается рекурсивно для двух полученных частей.
        /// Именно на этапе разделения элементов на группы в обычном алгоритме используется дополнительная память.
        /// Теперь разберёмся, как реализовать этот шаг in-place.
        /// Пусть мы как-то выбрали опорный элемент. Заведём два указателя left и right,
        /// которые изначально будут указывать на левый и правый концы отрезка соответственно.
        /// Затем будем двигать левый указатель вправо до тех пор, пока он указывает на элемент, меньший опорного.
        /// Аналогично двигаем правый указатель влево, пока он стоит на элементе, превосходящем опорный.
        /// В итоге окажется, что что левее от left все элементы точно принадлежат первой группе,
        /// а правее от right — второй.Элементы, на которых стоят указатели, нарушают порядок.
        /// Поменяем их местами (в большинстве языков программирования используется функция swap())
        /// и продвинем указатели на следующие элементы. Будем повторять это действие до тех пор, пока left и right не столкнутся.
        /// 
        /// </summary>
        /// <param name="listParticipant">Лист участников</param>
        /// <param name="start"></param>
        /// <param name="end"></param>
        static void InPlaceQuickSort(List<Participant> listParticipant, int start = 0, int end = 0)
        {
            void SortParticipant(int low, int high)
            {
                if (low >= high)
                {
                    return;
                }

                int left = low;
                int right = high;

                // необходимо выбрать опорный элемент (англ. pivot)
                Participant pivot = listParticipant[(right + left) / 2];

                while (left <= right)
                {
                    while (listParticipant[left].CompareTo(pivot) < 0)
                    {
                        left++;
                    }
                    while (listParticipant[right].CompareTo(pivot) > 0)
                    {
                        right--;
                    }

                    // swap
                    if (left <= right)
                    {
                        Swap(left, right);
                    }
                }

                SortParticipant(low, right);
                SortParticipant(left, high);
            }

            SortParticipant(start, end - 1);
        }

        static void Swap(int left, int right)
        {
            Participant tmp = listParticipant[left];
            listParticipant[left] = listParticipant[right];
            listParticipant[right] = tmp;

            left++;
            right--;

        }


        static void Main(string[] args)
        {
            int countPlayers = Convert.ToInt32(Console.ReadLine());

            List<Participant> players = new List<Participant>();

            // Чтение входных данных
            for (int i = 0; i < countPlayers; i++)
            {
                string[] input = Console.ReadLine().Split();

                string name = input[0];
                int countCompleteTask = Convert.ToInt32(input[1]);
                int finePoints = Convert.ToInt32(input[2]);

                players.Add(new Participant(name, countCompleteTask, finePoints));
            }

            InPlaceQuickSort(players, end: players.Count);

            // Вывод итога на экран
            foreach (Participant player in players)
            {
                Console.WriteLine(player.Name);
            }
        }
    }
    /// <summary>
    /// Класс участника
    /// </summary>
    class Participant : IComparable<Participant>
    {
        /// <summary>
        /// Имя
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// Количество решенных задач
        /// </summary>
        public int CountCompleteTask { get; set; }

        /// <summary>
        /// Штрафные очки (Fine Points)
        /// </summary>
        public int FinePoints { get; set; }

        /// <summary>
        /// Конструктор класса участника
        /// </summary>
        /// <param name="name">Имя участника</param>
        /// <param name="countCompleteTask">Количество решенных задач</param>
        /// <param name="finePoints">Штрафные очки</param>
        public Participant(string name, int countCompleteTask, int finePoints)
        {
            Name = name;
            CountCompleteTask = countCompleteTask;
            FinePoints = finePoints;
        }

        /// <summary>
        /// Метод сравнения участников по очкам, штрафным очкам, имени
        /// </summary>
        /// <param name="other"></param>
        /// <returns></returns>
        public int CompareTo(Participant other)
        {
            if (other == null)
            {
                return 1;
            }
            // Сравнение по количеству решенных задач
            // при сравнении двух участников выше будет идти тот,
            // у которого решено больше задач.
            if (CountCompleteTask != other.CountCompleteTask)
            {
                return other.CountCompleteTask.CompareTo(CountCompleteTask);
            }
            // Сравнение по штрафным очкам
            // При равенстве числа решённых задач
            // первым идёт участник с меньшим штрафом.
            else if (FinePoints != other.FinePoints)
            {
                return FinePoints.CompareTo(other.FinePoints);
            }
            // Сравнение по имени
            // Если же и штрафы совпадают, то первым будет тот,
            // у которого логин идёт раньше в алфавитном (лексикографическом) порядке.
            else
            {
                return Name.CompareTo(other.Name);
            }
        }
    }
}
