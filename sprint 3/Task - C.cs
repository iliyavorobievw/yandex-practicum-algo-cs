using System;

namespace ConsoleApp1
{
    class Program
    {
        static void Main(string[] args)
        {
            string substring = Console.ReadLine();
            string str = Console.ReadLine();
            Console.WriteLine(CustomContains(substring, str));
        }

        static bool CustomContains(string substring, string str)
        {
            int position = -1;
            foreach (char i in substring)
            {
                position = str.IndexOf(i, position + 1);
                if (position == -1)
                {
                    return false;
                }
            }
            return true;
        }
    }
}
