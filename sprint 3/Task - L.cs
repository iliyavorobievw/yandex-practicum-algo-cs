using System;
using System.Linq;
using System.Collections;
using System.Collections.Generic;
using System.Security.Cryptography.X509Certificates;
using System.Globalization;
using System.Runtime.Intrinsics.X86;
using System.Diagnostics;

public class Program
{
    static void Main(string[] args)
    {

        var allCount = Console.ReadLine();
        var array = Console.ReadLine().Split(' ').Select(x => int.Parse(x)).ToList();
        var price = int.Parse(Console.ReadLine());


        var firstDay = array.FindIndex(x => x >= price);
        var secondDay = array.FindIndex(x => x >= price * 2);
        if (firstDay >= 0)
        {
            firstDay++;
        }
        if (secondDay >= 0)
        {
            secondDay++;
        }

        Console.WriteLine(firstDay + " " + secondDay);

    }

}
