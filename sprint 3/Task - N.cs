using System;
using System.Collections.Generic;

namespace ConsoleApp1
{
    class Program
    {
        static void Main(string[] args)
        {
            int count_line = int.Parse(Console.ReadLine());
            List<int[]> flower_mans = new List<int[]>();
            for (int i = 0; i < count_line; i++)
            {
                string[] input = Console.ReadLine().Split();
                int start = int.Parse(input[0]);
                int end = int.Parse(input[1]);
                flower_mans.Add(new int[] { start, end });
            }

            flower_mans.Sort((x, y) => x[0].CompareTo(y[0]));
            List<int[]> results = new List<int[]>();
            int idx = 0;
            int big_start = flower_mans[idx][0];
            int big_end = flower_mans[idx][1];
            idx += 1;
            while (idx < count_line)
            {
                if (big_start <= flower_mans[idx][0] && flower_mans[idx][0] <= big_end)
                {
                    int curr_end = flower_mans[idx][1];
                    idx += 1;
                    if (curr_end > big_end)
                    {
                        big_end = curr_end;
                    }
                }
                else
                {
                    results.Add(new int[] { big_start, big_end });
                    big_start = flower_mans[idx][0];
                    big_end = flower_mans[idx][1];
                    idx += 1;
                }
            }
            results.Add(new int[] { big_start, big_end });

            foreach (int[] res in results)
            {
                Console.WriteLine(string.Join(" ", res));
            }
        }
    }
}
