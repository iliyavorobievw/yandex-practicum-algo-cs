using System;

namespace ConsoleApp1
{
    class Program
    {
        static void Main(string[] args)
        {
            int bracketCount = int.Parse(Console.ReadLine());
            int control = 0;
            int n1 = bracketCount;
            int n2 = bracketCount;
            GenerateBracketSeq(control, n1, n2, "");
        }

        static void GenerateBracketSeq(int control, int n1, int n2, string prefix)
        {
            if (n1 == 0 && n2 == 0)
            {
                Console.WriteLine(prefix);
            }
            else
            {
                if (n1 > 0)
                {
                    GenerateBracketSeq(control + 1, n1 - 1, n2, prefix + "(");
                }
                if (control > 0 && n2 > 0)
                {
                    GenerateBracketSeq(control - 1, n1, n2 - 1, prefix + ")");
                }
            }
        }
    }
}
