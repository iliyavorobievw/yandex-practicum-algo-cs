using System;
using System.Collections.Generic;
using System.Linq;

class Program
{
    static void Main(string[] args)
    {
        var n = int.Parse(Console.ReadLine());
        var words = Console.ReadLine().Split();

        var groups = GroupAnagrams(words);

        foreach (var group in groups.OrderBy(g => g.First()))
        {
            Console.WriteLine(string.Join(" ", group));
        }
    }

    static List<List<int>> GroupAnagrams(string[] words)
    {
        var groups = new Dictionary<string, List<int>>();

        for (var i = 0; i < words.Length; i++)
        {
            var word = words[i];
            var key = String.Concat(word.OrderBy(c => c));

            if (!groups.ContainsKey(key))
            {
                groups[key] = new List<int>();
            }

            groups[key].Add(i);
        }

        return groups.Values.ToList();
    }
}
