using System;
using System.Linq;
using System.Collections.Generic;

class Program
{
    static void Main(string[] args)
    {
        int count = Convert.ToInt32(Console.ReadLine());

        List<string> sections = new List<string>();

        for (int i = 0; i < count; i++)
        {
            sections.Add(Console.ReadLine());
        }

        sections = sections.Distinct().ToList();

        foreach (string section in sections)
        {
            Console.WriteLine(section);
        }
    }
}
