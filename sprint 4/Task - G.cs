using System;
using System.Collections.Generic;
using System.Linq;

namespace ConsoleApp
{
    class Program
    {
        static void Main(string[] args)
        {
            int n = int.Parse(Console.ReadLine());
            if (n == 0)
            {
                Console.WriteLine(0);
                return;
            }

            int[] rounds = Console.ReadLine().Split().Select(int.Parse).ToArray();

            int maxLen = 0;
            int sum = 0;
            Dictionary<int, int> firstOccurrence = new Dictionary<int, int>();

            for (int i = 0; i < n; i++)
            {
                sum += rounds[i] == 0 ? -1 : 1;
                if (sum == 0)
                {
                    maxLen = i + 1;
                }
                else
                {
                    if (firstOccurrence.ContainsKey(sum))
                    {
                        maxLen = Math.Max(maxLen, i - firstOccurrence[sum]);
                    }
                    else
                    {
                        firstOccurrence[sum] = i;
                    }
                }
            }

            Console.WriteLine(maxLen);
        }
    }
}
