using System;
using System.Collections.Generic;

namespace ConsoleApp
{
    class Program
    {
        static void Main(string[] args)
        {
            var s = Console.ReadLine();
            var t = Console.ReadLine();

            if (CrazyCompare(s, t))
            {
                Console.WriteLine("YES");
            }
            else
            {
                Console.WriteLine("NO");
            }
        }

        public static bool CrazyCompare(string s, string t)
        {
            var i = 0;
            var dictionary = new Dictionary<char, char>();

            foreach (char c in s)
            {
                if (!dictionary.ContainsKey(c))
                {
                    if (i >= t.Length)
                    {
                        return false;
                    }
                    if (dictionary.ContainsValue(t[i]))
                    {
                        return false;
                    }

                    dictionary[c] = t[i];

                    i++;

                }
                else
                {
                    if (i >= t.Length || dictionary[c] != t[i])
                    {
                        return false;
                    }

                    i++;
                }
            }

            return i == t.Length;
        }
    }
}
