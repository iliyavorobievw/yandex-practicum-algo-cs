using System;

class Program
{
    static int SearchMaxSubStr(string input)
    {
        var maxSubWord = "";

        var count = 0;

        var endIndex = input.Length - 1;

        foreach (char c in input)
        {
            if (!maxSubWord.Contains(c))
            {
                maxSubWord += c;
            }
            else
            {
                if (count < maxSubWord.Length)
                {
                    count = maxSubWord.Length;
                }
                maxSubWord = maxSubWord.Substring(maxSubWord.IndexOf(c) + 1) + c;
            }
        }

        return count > maxSubWord.Length ? count : maxSubWord.Length;
    }

    static void Main(string[] args)
    {
        var inputString = Console.ReadLine();

        Console.WriteLine(SearchMaxSubStr(inputString));
    }
}
