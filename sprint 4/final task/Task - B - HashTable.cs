﻿using System;
using System.Collections.Generic;

/*
 * report link  -  https://contest.yandex.ru/contest/24414/run-report/87980378/
 * 
 * Реализация простой хэш-таблицы
 * с разрешением коллизий методом цепочек.
 * 
 * Временная сложность всех операций в среднем составляет O(1).
 * Пространственная сложность составляет O(n), где n - это количество элементов в таблице.
 * 
 * Алгоритм работает следующим образом:
 * Мы создаем массив списков для хранения элементов хэш-таблицы.
 * 
 * 
 * Для добавления элемента в таблицу мы вычисляем его хэш
 * и добавляем его в соответствующий список.
 * 
 * Если элемент с таким ключом уже есть в таблице,
 * то мы обновляем его значение. 
 * 
 * Для получения значения по ключу мы вычисляем хэш ключа
 * и ищем элемент с таким ключом в соответствующем списке.
 * 
 * Если элемент найден, то мы возвращаем его значение, иначе возвращаем None. 
 * 
 * Для удаления элемента из таблицы мы вычисляем его хэш
 * и ищем элемент с таким ключом в соответствующем списке.
 * 
 * Если элемент найден, то - удалить и вернуть его значение,
 * else возвращаем None.
 * 
 * Один из способов ускорения кода - это увеличение размера массива
 * для хранения элементов хэш-таблицы.
 * Это позволит уменьшить количество коллизий и ускорить выполнение операций с таблицей.
 * 
 */

class Program
{
    static void Main(string[] args)
    {
        // Создаем новую хэш-таблицу
        var myCustomHashTable = new MyCustomHashTable();

        // Читаем количество команд
        var countCommand = int.Parse(Console.ReadLine());

        for (int i = 0; i < countCommand; i++)
        {
            // чтение команды и её аргументов
            var command = Console.ReadLine().Split();
            var operation = command[0];
            var key = int.Parse(command[1]);

            // ИИ на if-ах для работы с HashTable
            if (operation == "put")
            {
                var value = int.Parse(command[2]);
                myCustomHashTable.Put(key, value);
            }
            else if (operation == "get")
            {
                Console.WriteLine(myCustomHashTable.Get(key));
            }
            else if (operation == "delete")
            {
                Console.WriteLine(myCustomHashTable.Delete(key));
            }
        }
    }
}
/// <summary>
/// Своя имплементация hashTable
/// </summary>
class MyCustomHashTable
{
    // Размер массива
    private const int size = 100000;

    // Сообщение 
    private const string errorMessage = "None";

    // Массив для хэш-таблицы
    private List<KeyValuePair<int, int>>[] table;
    /// <summary>
    /// ctor
    /// </summary>
    public MyCustomHashTable()
    {
        // Инициализируем массив пустыми списками
        table = new List<KeyValuePair<int, int>>[size];
    }
    /// <summary>
    /// добавление пары ключ-значение.
    /// </summary>
    /// <param name="key">ключ</param>
    /// <param name="value">значение</param>
    public void Put(int key, int value)
    {
        // Вычисляем индекс в массиве для key
        var index = GetIndex(key);

        // Если по этому индексу нет списка, то создаем его
        if (table[index] == null)
        {
            table[index] = new List<KeyValuePair<int, int>>();
        }

        // Ищем элемент с key в списке и удаляем его, если он есть
        foreach (var pair in table[index])
        {
            if (pair.Key == key)
            {
                table[index].Remove(pair);
                break;
            }
        }

        // Добавляем новый элемент в список
        table[index].Add(new KeyValuePair<int, int>(key, value));
    }
    /// <summary>
    /// получение значения по ключу.
    /// </summary>
    /// <param name="key">ключ</param>
    /// <returns></returns>
    public string Get(int key)
    {
        // Вычисляем индекс в массиве для заданного ключа
        var index = GetIndex(key);
        // Если список по этому индексу существует
        if (table[index] != null)
        {
            // Ищем элемент с заданным ключом в списке и возвращаем его значение, если он есть
            foreach (var pair in table[index])
            {
                if (pair.Key == key)
                {
                    return pair.Value.ToString();
                }
            }
        }

        // Если элемент не найден, то возвращаем errorMessage
        return errorMessage;
    }
    /// <summary>
    /// удаление ключа из таблицы.
    /// </summary>
    /// <param name="key">ключ</param>
    /// <returns></returns>
    public string Delete(int key)
    {
        // Вычисляем индекс в массиве для заданного ключа
        var index = GetIndex(key);

        // Если список по этому индексу существует
        if (table[index] != null)
        {
            // Ищем элемент с заданным ключом в списке и удаляем его, если он есть
            foreach (var pair in table[index])
            {
                if (pair.Key == key)
                {
                    table[index].Remove(pair);
                    return pair.Value.ToString();
                }
            }
        }

        // Если элемент не найден, то возвращаем errorMessage
        return errorMessage;
    }
    /// <summary>
    /// вычисление индекса в массиве по ключу
    /// </summary>
    /// <param name="key">ключ</param>
    /// <returns></returns>
    private int GetIndex(int key)
    {
        return Math.Abs(key) % size;
    }
}
