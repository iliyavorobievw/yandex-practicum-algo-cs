using System;
using System.Collections.Generic;
using System.Linq;


// report link - https://contest.yandex.ru/contest/24414/run-report/87977171/

/*
 * Временная сложность этого алгоритма составляет O(nk + mq),
 * 
 * n - это количество документов, k - средняя длина документа в словах,
 * m - количество запросов и q - средняя длина запроса в словах.
 * 
 * Пространственная сложность составляет O(nk),
 * 
 * так как мы храним индекс для всех слов во всех документах.
 * 
 * 
 * Работа алгоритма:
 * 
 * Читаем документы и строим поисковую систему.
 * Индекс - словарь, Key - слова,
 * Value - словари с информацией о том,
 * в каких документах и сколько раз встречается каждое слово.
 * Дальше читаем все запросы и для каждого запроса ищем самые релевантные документы.
 * Для этого мы считаем релевантность каждого документа,
 * Как сумму числа вхождений всех слов из запроса в этот документ.
 * Затем мы сортируем документы по убыванию релевантности и выводим первые 5.
 */

class Program
{
    static void Main(string[] args)
    {
        // Читаем количество документов
        var n = int.Parse(Console.ReadLine());
        var documents = new List<string>();
        for (var i = 0; i < n; i++)
        {
            // Читаем текст каждого документа
            documents.Add(Console.ReadLine());
        }

        // Строим поисковый индекс
        var index = BuildIndex(documents);

        // Читаем количество запросов
        var m = int.Parse(Console.ReadLine());
        for (var i = 0; i < m; i++)
        {
            // Читаем текст каждого запроса
            var query = Console.ReadLine();
            // Ищем самые релевантные документы
            var relevantDocuments = Search(index, query);
            // Выводим результат
            Console.WriteLine(string.Join(" ", relevantDocuments));
        }
    }

    // Функция для построения поискового индекса
    static Dictionary<string, Dictionary<int, int>> BuildIndex(List<string> documents)
    {
        // Создаем пустой индекс
        var index = new Dictionary<string, Dictionary<int, int>>();

        for (var i = 0; i < documents.Count; i++)
        {
            // Разбиваем текст документа на слова
            var document = documents[i];
            var words = document.Split();

            foreach (var word in words)
            {
                // Добавляем слово в индекс, если его там еще нет
                if (!index.ContainsKey(word))
                {
                    index[word] = new Dictionary<int, int>();
                }

                // Увеличиваем счетчик вхождений слова в документ
                if (!index[word].ContainsKey(i))
                {
                    index[word][i] = 0;
                }

                index[word][i]++;
            }
        }

        return index;
    }

    // Функция для поиска самых релевантных документов по запросу
    static List<int> Search(Dictionary<string, Dictionary<int, int>> index, string query)
    {
        // Разбиваем текст запроса на слова и удаляем дубликаты
        var words = new HashSet<string>(query.Split());
        // Создаем словарь для подсчета релевантности каждого документа
        var relevance = new Dictionary<int, int>();

        foreach (var word in words)
        {
            // Проверяем, есть ли слово в индексе
            if (index.ContainsKey(word))
            {
                foreach (var entry in index[word])
                {
                    // Увеличиваем релевантность документа на число вхождений слова в него
                    var documentId = entry.Key;
                    var count = entry.Value;

                    if (!relevance.ContainsKey(documentId))
                    {
                        relevance[documentId] = 0;
                    }

                    relevance[documentId] += count;
                }
            }
        }

        // Сортируем документы по убыванию релевантности и возрастанию номера и берем первые 5
        var relevantDocuments = relevance.OrderByDescending(r => r.Value).ThenBy(r => r.Key).Select(r => r.Key + 1).Take(5).ToList();
        return relevantDocuments;
    }
}
