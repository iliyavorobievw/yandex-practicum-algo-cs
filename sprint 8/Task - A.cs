using System;
using System.Collections.Generic;
using System.Linq;

public class Program
{
    public static void Main()
    {
        var str = Console.ReadLine().Split().Reverse();
        var newStr = "";
        foreach (var s in str)
        {
            newStr += s + " ";
        }
        Console.WriteLine(newStr.Trim());

    }
}
