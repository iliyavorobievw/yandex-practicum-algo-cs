using System;
using System.Text;

class Program
{
    static void Main()
    {
        string a = Console.ReadLine();
        string b = Console.ReadLine();

        a = FilterEvenChars(a);
        b = FilterEvenChars(b);

        Console.WriteLine(Compare(a, b));
    }

    static string FilterEvenChars(string s)
    {
        StringBuilder builder = new StringBuilder();
        foreach (char symbol in s)
        {
            if (char.IsWhiteSpace(symbol))
                break;
            if (symbol % 2 == 0)
                builder.Append(symbol);
        }
        return builder.ToString();
    }

    static string Compare(string a, string b)
    {
        if (a == b)
            return "0";
        for (int i = 0; i < a.Length; i++)
        {
            if (i == b.Length)
                return "1";
            if (a[i] == b[i])
                continue;
            if (a[i] < b[i])
                return "-1";
            else
                return "1";
        }
        return "-1";
    }
}