using System;
using System.Linq;

class Program
{
    static void Main()
    {
        int n = int.Parse(Console.ReadLine());
        string[] strings = new string[n];
        for (int i = 0; i < n; i++)
            strings[i] = Console.ReadLine();

        int prefixLength = strings.Min(s => s.Length);
        for (int i = 0; i < prefixLength; i++)
        {
            char c = strings[0][i];
            for (int j = 1; j < n; j++)
            {
                if (strings[j][i] != c)
                {
                    prefixLength = i;
                    break;
                }
            }
        }

        Console.WriteLine(prefixLength);
    }
}