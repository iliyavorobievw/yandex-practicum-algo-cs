using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

class Program
{
    static void Main()
    {
        string line = Console.ReadLine();

        // подсчитываем частоту каждого символа во входной строке
        Dictionary<char, int> counter = new Dictionary<char, int>();
        foreach (char symbol in line)
        {
            if (counter.ContainsKey(symbol))
                counter[symbol]++;
            else
                counter[symbol] = 1;
        }

        // все символы, имеющие чётную частоту (2n), будут частью ответа
        StringBuilder begin = new StringBuilder();
        StringBuilder end = new StringBuilder();
        string mid = "";

        for (char i = 'a'; i <= 'z'; i++)
        {
            if (!counter.ContainsKey(i) || counter[i] == 0)
                continue;

            // нечётная частота символа
            if (counter[i] % 2 == 1 && mid.Length == 0)
            {
                mid = i.ToString();
                counter[i]--;
            }

            // чётная частота символа
            for (int j = 0; j < counter[i] / 2; j++)
                begin.Append(i);
        }

        // end - это реверс от begin
        for (int i = begin.Length - 1; i >= 0; i--)
            end.Append(begin[i]);

        string palindrome = begin.ToString() + mid + end.ToString();

        Console.WriteLine(palindrome);
    }
}
