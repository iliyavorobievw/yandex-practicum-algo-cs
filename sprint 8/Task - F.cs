using System;
using System.Collections.Generic;
using System.Linq;

class Program
{
    static void Main(string[] args)
    {
        int n = int.Parse(Console.ReadLine());
        Dictionary<string, int> words = new Dictionary<string, int>();

        for (int i = 0; i < n; i++)
        {
            string word = Console.ReadLine();
            if (words.ContainsKey(word))
            {
                words[word]++;
            }
            else
            {
                words[word] = 1;
            }
        }

        var oftenWord = words
            .OrderByDescending(x => x.Value)
            .ThenBy(x => x.Key)
            .First();

        Console.WriteLine(oftenWord.Key);
    }
}
