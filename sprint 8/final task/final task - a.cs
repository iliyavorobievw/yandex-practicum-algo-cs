using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

class Program
{

/*
*** report-link - https://contest.yandex.ru/contest/26133/run-report/93221309/

** Алгоритм для поиска наибольшего общего префикса
для строк в запакованном виде.

описание работы алгоритма:

1) unpacking - string :
Алгоритм распаковывает строки с помощью стека,
в который добавляются символы строки по одному, когда встречается символ ],
Алгоритм начинает извлекать символы из стека и объединять их в строку до тех пор, пока не встретит число.
Это число указывает, сколько раз нужно повторить полученную строку.
Полученная строка с повторениями затем добавляется обратно в стек.
Процесс продолжается до тех пор,
пока не будут обработаны все символы входной строки.

2) Поиск общего префикса:
После распаковки всех строк алгоритм ищет наибольший общий префикс между ними.
Это делается путем сравнения символов каждой строки по одному начиная с первого символа.
Как только символы в двух строках начинают отличаться, алгоритм останавливается и возвращает индекс,
на котором начинаются различия.


Временная сложность - O(n),
где n - общее количество символов во всех строках.
Пространственная сложность - O(n),
поскольку в худшем случае все символы могут быть помещены в стек в функции UnpackString.
*/

    static void Main()
    {
        // Считывание числа строк из стандартного ввода
        var numLines = int.Parse(Console.ReadLine());
        // Распаковка первой строки и сохранение ее в общем префиксе
        var commonPrefix = UnpackString(Console.ReadLine());
        // Цикл по оставшимся строкам
        for (var i = 1; i < numLines; i++)
        {
            // Распаковка текущей строки
            var currentLine = UnpackString(Console.ReadLine());
            // Обновление общего префикса на основе общего префикса между текущим общим префиксом и распакованной строкой
            commonPrefix = currentLine.Substring(0, GetCommonPrefixIndex(commonPrefix, currentLine));
        }
        // Вывод общего префикса
        Console.WriteLine(commonPrefix);
    }

    static string UnpackString(string input)
    {
        // Инициализация стека для хранения символов и чисел
        var stack = new Stack<string>();
        // Цикл по каждому символу во входной строке
        for (var i = 0; i < input.Length; i++)
        {
            // Пропуск пробелов
            if (char.IsWhiteSpace(input[i]))
                continue;
            // Если текущий символ - это ']', то выполняется распаковка
            if (input[i] == ']')
            {
                var curr = "";
                // Извлечение всех символов из стека до тех пор, пока не будет найдено число
                while (!char.IsDigit(stack.Peek()[0]))
                    curr = stack.Pop() + curr;
                // Извлечение числа из стека и повторение строки соответствующее количество раз
                var numRepeats = int.Parse(stack.Pop());
                stack.Push(string.Concat(Enumerable.Repeat(curr, numRepeats)));
            }
            else if (input[i] != '[')
            {
                // Если текущий символ не '[', то он добавляется в стек
                stack.Push(input[i].ToString());
            }
        }
        // Возврат строки, полученной из элементов стека
        return string.Join("", stack.Reverse());
    }

    static int GetCommonPrefixIndex(string str1, string str2)
    {
        // Вычисление максимальной длины из двух строк
        var maxLength = Math.Max(str1.Length, str2.Length);
        var index = 0;
        // Поиск индекса, где строки начинают различаться
        while (index < maxLength)
        {
            if (index >= str1.Length || index >= str2.Length || str1[index] != str2[index])
                break;
            index++;
        }
        return index;
    }
}
