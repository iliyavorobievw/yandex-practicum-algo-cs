using System;
using System.Collections.Generic;

public class Program
{
    /*
    report-link - https://contest.yandex.ru/contest/26133/run-report/93233804/

    ***
    Пространственная сложность - O(n)
    n - это длина входного текста.

    Временная сложность -  O(nmk)
    n - длина входящего текста,
    m - число слов в списке,
    k - avg длина слова.
    Мы проходим по каждому символу в тексте и для каждого символа мы проверяем каждое слово в списке.
    На каждом слове - Substring, который занимает O(k) времени.


    ***Описание***
    Этот код сначала считывает текст и список слов.
    Затем он использует динамическое программирование для определения, можно ли разбить текст на слова из списка.
    Если это возможно, вывод -  “YES”, иначе - “NO”.
    */
    public static void Main()
    {
        // Считывание входных данных
        var text = Console.ReadLine();
        var n = int.Parse(Console.ReadLine());
        var words = new List<string>();

        for (int i = 0; i < n; i++)
        {
            words.Add(Console.ReadLine());
        }

        // Проверка, можно ли разбить текст на слова
        var canBeSplit = CanBeSplitIntoWords(text, words);
        Console.WriteLine(canBeSplit ? "YES" : "NO");
    }

    private static bool CanBeSplitIntoWords(string text, List<string> words)
    {
        // Инициализация массива dp
        var dp = new bool[text.Length + 1];
        dp[0] = true;

        // Проход по каждому символу в тексте
        for (int i = 1; i <= text.Length; i++)
        {
            // Проверка каждого слова в списке
            foreach (var word in words)
            {
                // Если слово может быть подстрокой текущего текста и текст до этого слова может быть разбит на слова
                if (word.Length <= i && dp[i - word.Length])
                {
                    // Если подстрока равна слову, устанавливаем dp[i] в true
                    if (text.Substring(i - word.Length, word.Length) == word)
                    {
                        dp[i] = true;
                        break;
                    }
                }
            }
        }

        // Возвращаем значение для последнего символа в тексте
        return dp[text.Length];
    }
}
